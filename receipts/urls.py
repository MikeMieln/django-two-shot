from receipts.views import home, create_receipt
from django.urls import path

urlpatterns = [
    path("", home, name="home"),
    path("create/", create_receipt, name="create_receipt"),
]
